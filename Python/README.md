# NRT-AC Python example code

## Execute
Make sure you have python 3 installed:
* Linux: `sudo apt install python3`
* Windows: [Python install](https://www.python.org/downloads/windows/)

Open a terminal in current directory and run the following command to start the example program
```
python3 tcp.py
```
When you run the program it will connect to the NRT-AC on IP *192.168.1.102* port *2002*. You can change the IP/Port in the code and rerun the code if you want to connect on another IP/port

