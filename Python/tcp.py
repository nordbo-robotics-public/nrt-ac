import socket
import sys
import time
import struct  # https://docs.python.org/3/library/struct.html#format-characters
import logging as L

IP_ADDRESS_NAC = '192.168.1.102'
PORT = 2002

def f(types):
    cvt = {
        'int8': 'b',
        'uint8': 'B',
        'int16': 'h',
        'uint16': 'H',
        'int32': 'i',
        'uint32': 'I',
        'int64': 'q',
        'uint64': 'Q',
        'float32': 'f',
        'double64': 'd'
    }
    return ''.join([cvt[t] for t in types])


def tx(sock, command, format=[], payload=0):
    try:
        iter(payload)
    except TypeError as e:
        payload = [payload]
    data = bytes() if len(format) == 0 else struct.pack('<' + f(format), *payload)
    length = 2 + len(data)

    L.debug(f'tx: {length=}, command=0x{command:x}, {payload=}')

    tx_packet = struct.pack('<' + f(['uint8', 'uint8']), length, command) + data
    sock.send(tx_packet)


def rx(sock, format=[], endian='<'):
    rx_packet = sock.recv(2)

    # If no package received
    if len(rx_packet) == 0:
        return -1

    # Get payload, if any
    rx_packet += sock.recv(rx_packet[0] - len(rx_packet))

    # Unpack it
    try:
        result = struct.unpack(endian + f(['uint8'] * 2) + f(format), rx_packet)
    except struct.error as e:
        L.error(str(e))
        L.info(f'{len(rx_packet)}: {rx_packet}')
        sys.exit(1)

    length = int(result[0])
    command = int(result[1])
    payload = [r for r in result[2:]]

    L.debug(f'rx: {length=}, command=0x{command:x}, {payload=}')

    return length, command, payload


def main():
    # Create socket object and connect
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((IP_ADDRESS_NAC, PORT))
    sock.settimeout(2.0)

    keep_going = False
    if 1 < len(sys.argv): # never keep going when running with commandline
        keep_going = False
    if keep_going:
        delay = 0.1  # [s]
    cmd = 'get_gravity' if len(sys.argv) < 2 else sys.argv[1]
    val = 10009 if len(sys.argv) < 3 else int(sys.argv[2])

    # Send command (only happens once if keep_going = False)
    while True:
        match cmd:
            case 'start':
                tx(sock, 0x01, ['uint8'], 1)
                rx(sock)
            case 'stop':
                tx(sock, 0x01, ['uint8'], 0)
                rx(sock)
            case 'set_target_force':
                tx(sock, 0x02, ['int32'], val)
                rx(sock)
            case 'retract':
                tx(sock, 0x03)
                rx(sock)
            case 'set_target_force_ramp_time':
                tx(sock, 0x05, ['uint32'], val)
                rx(sock)
            case 'set_load_weight':
                tx(sock, 0x0b, ['uint32'], val)
                rx(sock)
            case 'get_state':
                tx(sock, 0x0f)
                rx(sock, ['uint8'])
            case 'get_position':
                tx(sock, 0x16)
                rx(sock, ['uint32'])
            case 'get_monitors':
                tx(sock, 0x17)
                rx(sock, ['int32'] * 2)
            case 'get_force_min':
                tx(sock, 0x19)
                rx(sock, ['int32'])
            case 'set_force_min':
                tx(sock, 0x1a, ['int32'], val)
                rx(sock)
            case 'get_load_weight':
                tx(sock, 0x1b)
                rx(sock, ['uint32'])
            case 'get_position_raw':
                tx(sock, 0x1c)
                rx(sock, ['uint32'])
            case 'calibrate':
                tx(sock, 0x28)
                rx(sock)
            case 'get_is_moving':
                tx(sock, 0x29)
                rx(sock, ['uint8'])
            case 'get_force_scale_vars':
                tx(sock, 0x2a)
                rx(sock, ['int32'] * 3)
            case 'set_force_scale_vars':
                tx(sock, 0x2b, ['int32'] * 3, [3001, 351, -238])
                rx(sock)
            case 'get_firmware_version':
                tx(sock, 0x30)
                rx(sock, ['uint8', 'uint8', 'uint8'])
            case 'get_gravity':
                tx(sock, 0x37)
                rx(sock, ['int32'] * 3)
            case 'set_gravity':
                tx(sock, 0x38, ['int32'] * 3, [val, 0, 0])
                rx(sock)
            case 'calibrate_imu':
                tx(sock, 0x39)
                rx(sock)
                prev = None
                while True:
                    tx(sock, 0x37)
                    cur = rx(sock, ['int32'] * 3)
                    if prev is None:
                        prev = cur
                    if prev != cur:
                        break
                    time.sleep(0.1)
                L.info('Calibration finished')
            case 'get_imu_temperature':
                tx(sock, 0x40)
                rx(sock, ['uint32'])
            case 'get_imu_update_rate':
                tx(sock, 0x41)
                rx(sock, ['uint32'])
            case 'get_imu_quality_of_service':
                tx(sock, 0x42)
                rx(sock, ['uint8'])
            case 'get_error':
                tx(sock, 0x45)
                rx(sock, ['uint8'])
            case 'get_error_imu':
                tx(sock, 0x46)
                rx(sock, ['uint8'])
            case 'get_error_io_driver':
                tx(sock, 0x47)
                rx(sock, ['uint8'])
            case 'get_error_regulator':
                tx(sock, 0x48)
                rx(sock, ['uint8'])
            case 'get_error_distance_sensor':
                tx(sock, 0x49)
                rx(sock, ['uint8'])
            case _:
                L.error(f'{cmd=} does not match an implemented command.')

        if not keep_going:
            break
        time.sleep(delay)

    sock.close()


if __name__ == '__main__':
    L.basicConfig(level=L.DEBUG)
    main()
