# NRT-AC

Example code for controlling the Nordbo Active Compensation unit (NAC).

## Example code
Example code is located in their respective language folder. Instructions on how to compile and run it is described in the README file in each language folder.
* [Python example code](Python/README.md)
